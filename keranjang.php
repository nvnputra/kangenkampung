<!DOCTYPE html>
<html>
<link rel="stylesheet" href="../kangenkampung/css/kangenkampung.css">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick-theme.css"/>
<link rel="stylesheet" href="../kangenkampung/css/pygments.css" />
<link rel="stylesheet" href="../kangenkampung/css/easyzoom.css" />
<head>
	<title></title>
</head>
<body class="background-body">
	<nav class="navbar-alert fixed-top" id="navbar-alert">

		<div class="navbar-header navbar-pointer scroll-hide">
				    <div Class="navbar-left">
				    	<div class="navbar-contact">
                        Call us on (504) 305-0554
                    </div>
				    </div>
				    <div class="navbar-right">
				    	<div class="row text">
                            <div class="col-sm-1 mr7 ">
                                Home
                            </div>
                            <div class="col-sm-1 mr7 text">
                                About 
                            </div>
                            <div class="col-sm-1 text">
                                Contact
                            </div>
                    </div>
				    </div>
		  </div>

		 
		  <div class="navbar-body" >
		  	<div class="row">
		  		<div class="col-sm-2" style="margin-top: 15px;margin-left: 12px">
					  	<div class="logo-header" >
					  			<img src="../kangenkampung/images/logo.png">
					  	</div>	
		  		</div>
		  		<div class="col-sm-8" style="margin-top: 18px;">
				  	<div class="input-group">
					      <div class="input-group-prepend ">
										    <select name="product_cat" class="dropdown_product_cat navbar-pointer"><option value="">Select a category</option>	<option class="level-0" value="camera">Camera&nbsp;(4)</option>
												<option class="level-0" value="cooking">Cooking</option>
												<option class="level-0" value="fashion">Fashion</option>
												<option class="level-0" value="gadget">Gadget</option>
												<option class="level-0" value="helmet">Helmet</option>
												<option class="level-0" value="laptop">Laptop</option>
												<option class="level-0" value="sport" selected="selected">Sport</option>
											</select>
					      </div>
					      <input type="text" class="form-control search-box" placeholder="Cari Barang anda di sini ...">
					       <div class="input-group-prepend">
					       	 <span class="input-group-text search-icon"><i class="fa fa-search"></i></span>
					       </div>
					    </div>
				  	</div>
		  		<div class="col-sm-1"  style="margin-top: 11px;">
		  				<div class="btn-group">
					  <button type="button" class="navbar-login">Masuk</button>
					  <button type="button" class="navbar-daftar">Daftar</button>
					</div>
		  		s</div>
		  	</div>
		  </div>

		  <div class="appointment" style="text-align: left">
		  <div class="container">
		 		 Menu / home / Detail
		  </div>
		  </div>
	</nav>
	<div class="content">
		<div class="container" style="display: none;margin-top: 13%">
			<!-- jika kosong -->
			<div class="card cart-kosong">
				
			</div>
		</div>
		<div class="container"  style="margin-top: 13%">
		<div class="row">
			<div class="col-sm-8">
				<div class="card-cart-one">
					<div class="cart-price-header">
                		<div class="cart-price-header-text">Keranjang Belanja</div>
            		</div>
				
				<?php for ($i=0; $i <5 ; $i++) { ?>
				<div class="row cart-item">
					<div class="col-sm-8">
						<div class="row" style="text-align: left">
							<div class="col-sm-2">
							<div>
										<img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/8/19/1355640/1355640_50fcf146-d783-4d61-a178-1d3a8fe32bbd_1200_1006.jpg" class="cart-image-item">
									</div>
							</div>
							<div class="col-sm-10">
								<label style="font-weight: bold;">Samsung Galaxy A 10 (2 GB/32GB) - Blacksssss</label>
								<div style="font-weight: bold;margin-top: -10px" class="primary-color">Rp.20.000</div>
							</div>
							</div>
						</div>
					<div class="col-sm-4">
							<div class="row">
								<div class="col-sm-3">
									<div class="cart-remove-item"></div>
								</div>
								<div class="col-sm-9">
									<div class="row" >
										<div class="col-sm-3">
											<div class="order-min-button"></div>
										</div>
										<div class="col-sm-2">
											<label style="font-size: 23px" class="primary-color">2</label>
										</div>
										<div class="col-sm-3">
											<div class="order-plus-button"></div>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
				<hr>
				<?php }?>
			</div>
			</div>
			<div class="col-sm-4">
				<div class="card-cart-two">
					<div class="cart-price-header">
                		<div class="cart-price-header-text">Ringkasan Belanja</div>
            		</div>
            		<div class="card-price-cart">
            		<div class="row">
            			<div class="col-sm-6 cart-title-price-text" >Total Harga</div>
            			<div class="col-sm-6" style="text-align: right;font-weight: bold;">Rp.1.000.000</div>
            		</div>
            		</div>
            			<div>
            			<a href="checkout.php"><button class="btn btn-outline-success" style="margin-top: 8px;width: 100%">Beli</button></a>
            		</div>
				</div>
			</div>
		</div>
		</div>
	</div>		
</body>
<footer class="footer-div">
			<div>
					<img src="../kangenkampung/images/logo.png" class="logo-footer">
			</div>	
			<div style="margin-top: 2%">
				<label>120 Jasmine Ln, Westwego, LA, 70094</label><br>
				<label>(504) 305-0554 &nbsp;&nbsp;</label><label class="primary-color">hello@tumbas.com</label>
			</div>
			<div class="hr-footer">
				<hr>
			</div>		
				
			<div class="container">
			<div class="row">
				<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

				<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

					<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

				<div class="col-sm-4">
				</div>


				<div class="col-sm-4">
				</div>
			</div>
			</div>
</footer>

<!-- modal -->
<div>
	
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../kangenkampung/js/slick.min.js"></script>
	<script src="../kangenkampung/js/easyzoom.js"></script>
	<script src="../kangenkampung/js/kangenkampung.js"></script>
</html>

