<!DOCTYPE html>
<html>
<link rel="stylesheet" href="../kangenkampung/css/kangenkampung.css">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
<head>
	<title></title>
</head>
<body class="background-body">
	<nav class="navbar-alert fixed-top" id="navbar-alert" >

		<div class="navbar-header navbar-pointer scroll-hide close-overlay">
				    <div Class="navbar-left">
				    	<div class="navbar-contact">
                        Call us on (504) 305-0554
                    </div>
				    </div>
				    <div class="navbar-right">
				    	<div class="row">
                            <div class="col-sm-1 mr7">
                                Home
                            </div>
                            <div class="col-sm-1 mr7">
                                About 
                            </div>
                            <div class="col-sm-1">
                                Contact
                            </div>
                    </div>
				    </div>
		  </div>

		 
		  <div class="navbar-body" >
		  	<div class="row ">
		  		<div class="col-sm-2 " style="margin-top: 15px;margin-left: 12px">
					  	<div class="logo-header" >
					  			<img src="../kangenkampung/images/logo.png">
					  	</div>	
		  		</div>
		  		<div class="col-sm-8" style="margin-top: 18px;">
				  	<div class="input-group">
					      <div class="input-group-prepend ">
										    <select name="product_cat" class="dropdown_product_cat open-overlay "><option value="">Select a category</option>	<option class="level-0" value="camera">Camera&nbsp;(4)</option>
												<option class="level-0" value="cooking">Cooking&nbsp;(1)</option>
												<option class="level-0" value="fashion">Fashion&nbsp;(1)</option>
												<option class="level-0" value="gadget">Gadget&nbsp;(3)</option>
												<option class="level-0" value="helmet">Helmet&nbsp;(2)</option>
												<option class="level-0" value="laptop">Laptop&nbsp;(2)</option>
												<option class="level-0" value="sport" selected="selected">Sport&nbsp;(2)</option>
											</select>
					      </div>
					      <input type="text" class="form-control search-box close-overlay" placeholder="Cari Barang anda di sini ...">
					       <div class="input-group-prepend close-overlay navbar-pointer">
					       	 <span class="input-group-text search-icon"><i class="fa fa-search"></i></span>
					       </div>
					    </div>
				  	</div>
		  		<div class="col-sm-1 close-overlay"  style="margin-top: 11px;">
		  				<div class="btn-group">
						   	<button type="button" class="navbar-login" data-toggle="modal" data-target="#login-modal">Masuk</button>
						    <a href="register.php"><button type="button" class="navbar-daftar">Daftar</button></a>
					</div>
		  		s</div>
		  	</div>
		  </div>

		  <div class="appointment close-overlay">
		  	Fitur terbaru telah hadir segera rasakan
		  </div>
	</nav>
	<div class="content close-overlay" >
	<div id="overlay"></div>
	<div class="header-content">
	<div class="mt-3 ml-2">
		<div class="row " >
			<!-- Slide Carousel -->
			<div class="col-md-5 " style="margin-left: 4%">
			<div class="row">
			<div class="col-sm-12">
			<div class="card  image-card navbar-pointer" style="width:110%">
					<div id="demo" class="carousel slide" data-ride="carousel" >
				  <!-- Indicators -->
				  <ul class="carousel-indicators">
				    <li data-target="#demo" data-slide-to="0" class="active"></li>
				    <li data-target="#demo" data-slide-to="1"></li>
				    <li data-target="#demo" data-slide-to="2"></li>
				  </ul>
				  
				  <!-- The slideshow -->
				   
				  <div class="carousel-inner" style="">
				    <div class="carousel-item active" >
				   
				      <center><img src="https://thumbs.dreamstime.com/z/travel-promo-vector-banner-template-discount-text-travel-promo-vector-banner-template-discount-text-famous-tourist-119474228.jpg"  class="image-card-caraousel">
				      </center>
				    </div>
				    <div class="carousel-item">
				      <img src="https://thumbs.dreamstime.com/z/travel-promo-vector-banner-template-discount-text-travel-promo-vector-banner-template-discount-text-famous-tourist-119474228.jpg"  class="image-card-caraousel" > 
				    </div>
				    <div class="carousel-item">
				      <img src="https://thumbs.dreamstime.com/z/travel-promo-vector-banner-template-discount-text-travel-promo-vector-banner-template-discount-text-famous-tourist-119474228.jpg" class="image-card-caraousel">
				    </div>
				  </div>
  
				  <!-- Left and right controls -->
				  <a class="carousel-control-prev" href="#demo" data-slide="prev">
				    <span class="carousel-control-prev-icon"></span>
				  </a>
				  <a class="carousel-control-next" href="#demo" data-slide="next">
				    <span class="carousel-control-next-icon"></span>
				  </a>
				</div>
				</div>
			</div>
			<!-- IDNT -->
			<div class="col-sm-12 " style="margin-left: 9%;margin-top: 3%">
				<div class="row">
				<div class="col-sm-2">
				</div>
				</div>		
			</div>
			</div>

			</div>
			<div class="col-md-4 " style="text-align: right;">
				<div class="row  navbar-pointer">
					<div class="col-sm-12" style="margin-left: 9%">
						<div class="card image-card">
							<img src="https://s1.bukalapak.com/uploads/	flash_banner/64914/s-824-392/Banner_2.jpg" class="image-card" style="width:100%;border-radius:2%" >
						</div>
					</div>
					<br>
					<div class="col-sm-12 " style="margin-left: 9%;margin-top: -3%">
						<div class="row  navbar-pointer">
							<div class="col-sm-6">
							<div class="card image-card">
								<img src="https://s1.bukalapak.com/uploads/flash_banner/69814/s-392-392/Banner_4.jpg" class="image-card" style="width:200px;border-radius:2%" >
							</div>
							</div>

							<div class="col-sm-6" style="margin-right: -8%">
							<div class="card image-card">
								<img src="https://s1.bukalapak.com/uploads/flash_banner/17414/s-392-392/Banner_4.jpg"  class="image-card" style="width:200px;border-radius:2%" >
							</div>
							</div>
						</div>
					</div>
				</div>
					

					
				</div>
			<div class="col-md-2 ml-4" style="text-align: right;height: 50%">
				<div class="card  navbar-pointer image-card">
						<img src="https://s1.bukalapak.com/uploads/flash_banner/65124/s-392-824/Banner_3.jpg" class="image-card" style="width:100%;border-radius:2%">
				</div>
			</div>		
		</div>
	</div>
	</div>
	<div class="content-body">
	<!-- Tranding product -->
		<div  style="background: #c1bbbb12;margin-top: 2%" >
		<div class="ml4percent mr4percent" >
		<div class="row" >
			<div class="col-sm-6" style="margin-top: 1%">
			<p class="title-kategory">Paling Sering Di beli</p>
			</div>
			<div class="col-sm-6 text-show-all primary-color" style="text-align: right;margin-top: 1%;cursor: pointer">
			<p  >Lihat Semua</p>
			</div>
			
		</div>
		</div>
		<!-- multuplie -->
		<div class="ml4percent mr4percent" style="margin-top: 2%">
		 <div class="row">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                            	<?php for ($i=0; $i <5 ; $i++) {?>
                                <div class="card-item-tranding">
                                   <div class="card">
										<div class="img-hover-zoom ">
												<img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/7/12/1618811/1618811_5011e015-f0c6-477a-b330-61f425bb6a11_719_873.jpeg" style="width: 100%">
												<div class="top-right"><div class="price-diskon">26% OFF</div></div>
										</div>
						 				<div style="text-align: left;padding: 6px">
						 						<div style="" class="title-item"><b>[Premium Quality] Case Xiaomi Redmi Note 7</b></div>
						 						<div style="" class="primary-color price-item"><b>Rp.2222</b></div>
						 						<div style="" class="location-item">Jakarta</div>
					 	 				</div>

					 	 		</div>
					 	 		<br>
                                </div> 
                                <?php } ?>
                            </div>
                        </div>



                        <div class="carousel-item">
                            <div class="row">
                            <?php for ($i=0; $i <5 ; $i++) {?>
                                 <div class="card-item-tranding">
                                   <div class="card">
										<div class="img-hover-zoom ">
												<img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/7/12/1618811/1618811_5011e015-f0c6-477a-b330-61f425bb6a11_719_873.jpeg" style="width: 100%">
												<div class="top-right"><div class="price-diskon">26% OFF</div></div>
										</div>
						 				<div style="text-align: left;padding: 6px">
						 						<div style="" class="title-item"><b>[Premium Quality] Case Xiaomi Redmi Note 7</b></div>
						 						<div style="" class="primary-color price-item"><b>Rp.2222</b></div>
						 						<div style="" class="location-item">Jakarta</div>
					 	 				</div>

					 	 		</div>
                                </div>  
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div> 
		</div>
		</div>
		<!-- End Tranding product -->
		</div>
		<div>
		<div style="margin-top: 2%" >
		<div class="ml4percent mr4percent" >
		<div class="row">
			<div class="col-sm-6" >
			<p  class="title-kategory">Rekomendasi Untuk kamu</p>
			</div>
		</div>
		</div>
		<!-- multuplie -->
		<div class="ml4percent mr4percent">
		<div class="row">
		<?php for ($i=0; $i <10 ; $i++) {?>
		
		<div class="card-item">
		
			<div class="card">
			<a href="detail.php">
			<div class="img-hover-zoom ">
			<img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/3/12/26001163/26001163_c689c167-c903-4130-8f50-11a5cd481677_2048_2048.jpg" style="width: 100%">
						<div class="top-right"><div class="price-diskon">26% OFF</div></div>

			</div>
			 <div style="text-align: left;padding: 6px">
			 <div style="" class="title-item"><b>[Premium Quality] Case Xiaomi Redmi Note 7</b></div>
			 <span class="price-diskon-text">Rp.42.000</span>
			 <div style="" class="primary-color price-item"><b>Rp.60.000</b></div>
			 <div style="" class="location-item">jakarta</div>
		 	 </div>
		 	 </a>
		 	 </div>
		</div>
		
		<?php } ?>

		</div>
		</div>
		</div>
		</div>
		</div>
	</div>
	</div>
</body>
<footer class="footer-div">
			<div>
					<img src="../kangenkampung/images/logo.png" class="logo-footer">
			</div>	
			<div style="margin-top: 2%">
				<label>120 Jasmine Ln, Westwego, LA, 70094</label><br>
				<label>(504) 305-0554 &nbsp;&nbsp;</label><label class="primary-color">hello@tumbas.com</label>
			</div>
			<div class="hr-footer">
				<hr>
			</div>		
				
			<div class="container">
			<div class="row">
				<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

				<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

					<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

				<div class="col-sm-4">
				</div>


				<div class="col-sm-4">
				</div>
			</div>
			</div>
</footer>

<div>
	<!-- modal -->

	<!-- mdoal -login -->

  <!-- Modal -->
  <div class="modal fade" id="login-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered" style="width: 25%">
    
      <!-- Modal content-->
     
      <div class="modal-content">
       <div class="card-modal">
        <div class="modal-body">
        <div class="login-modal-header">
        	<div class="row">
        			<div class="col-sm-12">
        				<div>
        					<div class="row">
        					<div class="col-sm-6">
        						<label style="font-size: 22px;font-weight: bold">Masuk</label>
        					</div>
        					<div class="col-sm-6" style="text-align: right;">
        						<label class="primary-color" style="font-size: 12px;font-weight: bold;">Daftar</label>
        					</div>
        						
        						
        					</div>
        				</div>

        				<div>
        					<label  style="font-size: 12px;">Nomor Ponsel atau Email</label>
        					<input type="text" name="" class="form-control">
        					<label  style="font-size: 11px;color: grey">Contoh: email@kangenkamu.com</label>
        				</div>

        				<div>
        					<button class="btn btn-outline-success" style="width: 100%">Masuk</button>
        				</div>
        			</div>

        	</div>
        </div>
         
        </div>
      </div>
      </div>
      
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script src="../kangenkampung/js/kangenkampung.js"></script>
</html>

