<!DOCTYPE html>
<html>
<link rel="stylesheet" href="../kangenkampung/css/kangenkampung.css">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick-theme.css"/>
<link rel="stylesheet" href="../kangenkampung/css/pygments.css" />
<link rel="stylesheet" href="../kangenkampung/css/easyzoom.css" />
<head>
	<title></title>
</head>
<body class="background-body">
	<nav class="navbar-alert fixed-top" id="navbar-alert">

		<div class="navbar-header navbar-pointer scroll-hide">
				    <div Class="navbar-left">
				    	<div class="navbar-contact">
                        Call us on (504) 305-0554
                    </div>
				    </div>
				    <div class="navbar-right">
				    	<div class="row text">
                            <div class="col-sm-1 mr7 ">
                                Home
                            </div>
                            <div class="col-sm-1 mr7 text">
                                About 
                            </div>
                            <div class="col-sm-1 text">
                                Contact
                            </div>
                    </div>
				    </div>
		  </div>

		 
		  <div class="navbar-body" >
		  	<div class="row">
		  		<div class="col-sm-2" style="margin-top: 15px;margin-left: 12px">
					  	<div class="logo-header" >
					  			<img src="http://demoapus-wp.com/tumbas/wp-content/themes/tumbas/images/logo.png">
					  	</div>	
		  		</div>
		  		<div class="col-sm-8" style="margin-top: 18px;">
				  	<div class="input-group">
					      <div class="input-group-prepend ">
										    <select name="product_cat" class="dropdown_product_cat navbar-pointer"><option value="">Select a category</option>	<option class="level-0" value="camera">Camera&nbsp;(4)</option>
												<option class="level-0" value="cooking">Cooking</option>
												<option class="level-0" value="fashion">Fashion</option>
												<option class="level-0" value="gadget">Gadget</option>
												<option class="level-0" value="helmet">Helmet</option>
												<option class="level-0" value="laptop">Laptop</option>
												<option class="level-0" value="sport" selected="selected">Sport</option>
											</select>
					      </div>
					      <input type="text" class="form-control search-box" placeholder="Cari Barang anda di sini ...">
					       <div class="input-group-prepend">
					       	 <span class="input-group-text search-icon"><i class="fa fa-search"></i></span>
					       </div>
					    </div>
				  	</div>
		  		<div class="col-sm-1"  style="margin-top: 11px;">
		  				<div class="btn-group">
					  <button type="button" class="navbar-login">Masuk</button>
					  <button type="button" class="navbar-daftar">Daftar</button>
					</div>
		  		s</div>
		  	</div>
		  </div>

		  <div class="appointment" style="text-align: left">
		  <div class="container">
		 		 Menu / home / Detail
		  </div>
		  </div>
	</nav>
	<div class="content">
		
		<div class="container"  style="margin-top: 13%">
		<div class="row">
			<div class="col-sm-8">
				<div class="card-cart-one">
					<div class="checkout-text-header">
                		<div class="checkout-header-text-title">Checkout</div>
                		<label class="checkout-header-text">Alamat Pengirim</label>
            		</div>

            		<div style="margin-top: 2%">
            		<div class="row">
            			<div class="col-sm-6">
            				<p class="text-name-checkout">Alenovan Wiradhika Putra</p>
	            			<p class="text-phone-checkout">081334367717</p>
	            			<p class="text-alamat-checkout">Perumahan Junrejo indah no a25 pojok 
							   Junrejo, Kota Batu, 65321</p>
	            			</div>
            			<div class="col-sm-6">
            				<div class="col-sm-12" style="">
            					<label class="text-name-checkout"> Jasa Pengiriman</label>

            					<select class="form-control">
            						
            					</select>
            				</div>
            			</div>
            		</div>
            			
            		</div>
            		<div>
            			<div class="row">
            				<div class="col-sm-3">
            					<button class="btn btn-outline-success btn-sm"  data-toggle="modal" data-target="#alamat-modal">Ganti Alamat Tujuan</button>
            				</div>
            			</div>
            		</div>
				
				
				</div>

			<div class="card-cart-one" style="margin-top: 14px">
				<div class="col-sm-12">
				<div class="">
					<div class="cart-price-header">
                		<div class="cart-price-header-text">Keranjang Belanja</div>
            		</div>
				
				<?php for ($i=0; $i < 5 ; $i++) { ?>
				<div class="row cart-item">
					<div class="col-sm-8">
						<div class="row" style="text-align: left">
							<div class="col-sm-2">
							<div>
										<img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/8/19/1355640/1355640_50fcf146-d783-4d61-a178-1d3a8fe32bbd_1200_1006.jpg" class="cart-image-item">
									</div>
							</div>
							<div class="col-sm-10">
								<label style="font-weight: bold;">Samsung Galaxy A 10 (2 GB/32GB) - Blacksssss</label>
								<div style="font-weight: bold;margin-top: -10px" class="primary-color">Rp.20.000</div>
							</div>
							</div>
						</div>
					<div class="col-sm-4">
							<div class="row">
								<div class="col-sm-3">
									<div class="cart-remove-item"></div>
								</div>
								<div class="col-sm-9">
									<div class="row" >
										<div class="col-sm-3">
											<div class="order-min-button"></div>
										</div>
										<div class="col-sm-2">
											<label style="font-size: 23px" class="primary-color">2</label>
										</div>
										<div class="col-sm-3">
											<div class="order-plus-button"></div>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
				<hr>
				<?php }?>
			</div>
			</div>
			</div>
			</div>
			<div class="col-sm-4">
				<div class="card-cart-two">
					<div class="cart-price-header">
                		<div class="cart-price-header-text">Ringkasan Belanja</div>
            		</div>
            		<div class="card-price-cart">
            		<div class="row">
            			<div class="col-sm-6 cart-title-price-text" >Total Harga</div>
            			<div class="col-sm-6" style="text-align: right;font-weight: bold;">Rp.1.000.000</div>
            		</div>

            		<div class="row" style="margin-top: 4px">
            			<div class="col-sm-6 cart-title-price-text" >Ongkos kirim</div>
            			<div class="col-sm-6" style="text-align: right;font-weight: bold;">Rp.1.000.000</div>
            		</div>
            		</div>
            			<div>

            			<div class="row" style="margin-top: 7px">
            			<div class="col-sm-6 cart-title-price-text" style="font-weight: bold;font-size: 20px"" >Total</div>
            			<div class="col-sm-6 primary-color" style="text-align: right;font-weight: bold;font-size: 20px">Rp.1.000.000</div>
            		</div>
            		<div class="" style="margin-top: 5px"><button class="btn btn-outline-success" style="margin-top: 8px;width: 100%"  data-toggle="modal" data-target="#checkout-modal">Pembayaran</button>
            		</div>
            		</div>
				</div>
			</div>
		</div>
		</div>
	</div>		
</body>
<footer class="footer-div">
			<div>
					<img src="http://demoapus-wp.com/tumbas/wp-content/themes/tumbas/images/logo.png" class="logo-footer">
			</div>	
			<div style="margin-top: 2%">
				<label>120 Jasmine Ln, Westwego, LA, 70094</label><br>
				<label>(504) 305-0554 &nbsp;&nbsp;</label><label class="primary-color">hello@tumbas.com</label>
			</div>
			<div class="hr-footer">
				<hr>
			</div>		
				
			<div class="container">
			<div class="row">
				<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

				<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

					<div class="col-sm-4" style="text-align: left">
					<ul >
						<ol style="padding-top: 5%">About</ol>
						<ol style="padding-top: 5%">Contact</ol>
						<ol style="padding-top: 5%">Careers</ol>
						<ol style="padding-top: 5%">Press Releases</ol>
						<ol style="padding-top: 5%">In The Media</ol>
						<ol style="padding-top: 5%">Testimonials</ol>
					</ul>
				</div>

				<div class="col-sm-4">
				</div>


				<div class="col-sm-4">
				</div>
			</div>
			</div>
</footer>

<!-- modal -->

<!-- lokasi pengiriman -->
<div>
	<div>
		<div class="modal fade" id="alamat-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered" style="width: 30%">
    
      <!-- Modal content-->
     
      <div class="modal-content">
       <div class="checkout-card-modal">
        <div class="">
        <div class="" style="">
        <div class="row">
        	<div class="col-sm-2" style="margin-left: 6px">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        	</div>
        	<div class="col-sm-9">
        		<label class="title-checkout-modal">Alamat Tujuan</label>
        	</div>
        </div>
        
        </div>
      </div>
        <div class="modal-body">
        <div class="checkout-modal-header">
        	<div class="row">
        		<div class="col-sm-12">
        			<div>
        					<label  style="font-size: 12px;">Alamat Lengkap</label>
        					<textarea class="form-control">
        						
        					</textarea>
        					<label  style="font-size: 11px;color: grey">Contoh: Perumahan Junrejo indah no a25 pojok Junrejo, Kota Batu, 65321</label>
        					<div style="height: 10px"></div>
        				</div>
        		</div>
        	</div>
        </div>
         
        </div>
      </div>
      </div>
      
    </div>
  </div>
	</div>
</div>
<!-- login -->
<div>
	<div class="modal fade" id="login-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered" style="width: 25%">
    
      <!-- Modal content-->
     
      <div class="modal-content">
       <div class="card-modal">
        <div class="modal-body">
        <div class="login-modal-header">
        	<div class="row">
        			<div class="col-sm-12">
        				<div>
        					<div class="row">
        					<div class="col-sm-6">
        						<label style="font-size: 22px;font-weight: bold">Masuk</label>
        					</div>
        					<div class="col-sm-6" style="text-align: right;">
        						<label class="primary-color" style="font-size: 12px;font-weight: bold;">Daftar</label>
        					</div>
        						
        						
        					</div>
        				</div>

        				<div>
        					<label  style="font-size: 12px;">Nomor Ponsel atau Email</label>
        					<input type="text" name="" class="form-control">
        					<label  style="font-size: 11px;color: grey">Contoh: email@kangenkamu.com</label>
        				</div>

        				<div>
        					<button class="btn btn-outline-success" style="width: 100%">Masuk</button>
        				</div>
        			</div>

        	</div>
        </div>
         
        </div>
      </div>
      </div>
      
    </div>
  </div>
</div>

<!-- pembayaran  -->

<div>
	<div class="modal fade" id="checkout-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered" style="width: 30%">
    
      <!-- Modal content-->
     
      <div class="modal-content">
       <div class="checkout-card-modal">
        <div class="">
        <div class="" style="">
        <div class="row">
        	<div class="col-sm-2 step-two" style="margin-left: 6px;display: none" >
        		<button type="button" class="close back-payment"><i class="fa fa-arrow-left fa-xs" aria-hidden="true" style="color: #9a9898;font-size: 20px"></i></button>
        	</div>
        	<div class="col-sm-2 step-one" style="margin-left: 6px;display: block" >
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        	</div>
        	<div class="col-sm-9" style="margin-left: -2px">
        		<label class="title-checkout-modal">Pembayaran</label>
        	</div>
        </div>
        
        </div>
      </div>
        <div class="modal-body">
        <div class="checkout-modal-header">
        	<div class="row step-one" >
        		<div class="col-sm-12">
        			<div class="card">
        				<div>
        					<div class="row">
        						<div class="col-sm-4" style="margin-top: 18px;margin-left: 14px">
        							<p style="font-weight: bold;font-size: 15px">Total Tagihan</p>
        						</div>
        						<div class="col-sm-7" style="margin-top: 15px;text-align: right;">
        							<p class="primary-color" style="font-weight: bold;font-size: 20px">Rp.20.000.000</p>	
        						</div>
        					</div>
        				</div>
        			</div>

        			<div class="card" style="margin-top: 7px;margin-bottom: 12px">
        			<div class="card-metode-pembayaran">
        				<div class="row" style="margin-top: 13px;margin-left: 7px;margin-bottom: 2px">
        					<div class="col-sm-2" style="margin-top: 12px">
        						<img src="https://ecs7.tokopedia.net/img/toppay/sprites/bca.png" style="width: 60px">
        					</div>

        					<div class="col-sm-6" style="margin-left: 19px">
        						<p style="font-weight:600;font-size: 14px;margin-top: 2px;">Bank Central Asia</p>
        						<p style="font-size: 13px;margin-top: -12px">Virtual Account</p>
        					</div>
        					<div class="col-sm-3" style="margin-top: 12px">
        						<button class="btn btn-outline-success btn-sm next-payment"><i class="fa fa-arrow-right fa-md" aria-hidden="true"></i></button>
        					</div>
        				</div>
        				</div>
        				<div class="card-metode-pembayaran">
        				<div class="row" style="margin-top: 13px;margin-left: 7px;margin-bottom: 2px">
        					<div class="col-sm-2" style="margin-top: 12px">
        						<img src="https://ecs7.tokopedia.net/img/toppay/sprites/bca.png" style="width: 60px">
        					</div>

        					<div class="col-sm-6" style="margin-left: 19px">
        						<p style="font-weight:600;font-size: 14px;margin-top: 2px;">Bank Central Asia</p>
        						<p style="font-size: 13px;margin-top: -12px">Virtual Account</p>
        					</div>
        					<div class="col-sm-3" style="margin-top: 12px">
        						<button class="btn btn-outline-success btn-sm next-payment"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        					</div>
        				</div>
        				</div>
        				<div class="card-metode-pembayaran">
        				<div class="row" style="margin-top: 13px;margin-left: 7px;margin-bottom: 2px">
        					<div class="col-sm-2" style="margin-top: 12px">
        						<img src="https://ecs7.tokopedia.net/img/toppay/sprites/bca.png" style="width: 60px">
        					</div>

        					<div class="col-sm-6" style="margin-left: 19px">
        						<p style="font-weight:600;font-size: 14px;margin-top: 2px;">Bank Central Asia</p>
        						<p style="font-size: 13px;margin-top: -12px">Virtual Account</p>
        					</div>
        					<div class="col-sm-3" style="margin-top: 12px">
        						<button class="btn btn-outline-success btn-sm next-payment"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        					</div>
        				</div>
        				</div>
        				
        				<div style="height: 12px"></div>
        			</div>
        		</div>
        	</div>

        	<div class="row step-two" style="display: none">
        			<div class="col-sm-12">
        			<div class="card">
        				<div>
        					<div class="row">
        						<div class="col-sm-4" style="margin-top: 18px;margin-left: 14px">
        							<p style="font-weight: bold;font-size: 15px">Total Tagihan</p>
        						</div>
        						<div class="col-sm-7" style="margin-top: 15px;text-align: right;">
        							<p class="primary-color" style="font-weight: bold;font-size: 20px">Rp.20.000.000</p>	
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="card" style="margin-top: 15px">
        				<div class="row card-detail-payment">
        						<div class="col-sm-4" style="margin-top: 18px;margin-left: 14px">
        							<p style="font-size: 15px">Briva</p>
        						</div>
        						<div class="col-sm-7" style="margin-top: 15px;text-align: right;">
        							<img src="https://ecs7.tokopedia.net/img/toppay/sprites/bca.png" style="width: 60px">
        						</div>
        				</div>
        				<div class="detail-text-payment">
        					<label>
        						1.Transaksi ini akan otomatis menggantikan tagihan BRIVA yang belum dibayar.<br>
								2.Dapatkan kode pembayaran setelah klik “Bayar”.
        					</label>
        				</div>
        			</div>
        			<div>
        				<div class="card" style="margin-top: 5%">
        					<a href="detail_checkout.php"><button class="btn btn-success" style="width: 100%">Bayar</button>
                            </a>
        				</div>
        			</div>
        			</div>
        			</div>
        	</div>
        </div>
         
        </div>
      </div>
      </div>
      
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../kangenkampung/js/slick.min.js"></script>
	<script src="../kangenkampung/js/easyzoom.js"></script>
	<script src="../kangenkampung/js/kangenkampung.js"></script>
</html>

