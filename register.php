<!DOCTYPE html>
<html>
<link rel="stylesheet" href="../kangenkampung/css/kangenkampung.css">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick-theme.css"/>
<link rel="stylesheet" href="../kangenkampung/css/pygments.css" />
<link rel="stylesheet" href="../kangenkampung/css/easyzoom.css" />
<head>
	<title></title>
</head>
<body class="body-register">
	<nav>
	<center>
		<div class="" style="margin-top: 2%">
					  			<img src="../kangenkampung/images/logo.png">
		</div>	
	</center>
	</nav>
	<div class="content-daftar">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="card-cart-one">
						<div style="text-align: center;margin-top: 2%">
							<label style="font-size: 23px;font-weight: bold;">Daftar Sekarang</label>
							<div style="color: grey">Sudah punya aku kangen.com ? <span class="primary-color">Masuk</span></div>
						</div>
						<div style="padding: 5%">
								<!-- email -->
								<div >
		        					<label  style="font-size: 12px;color: grey">Nomor Ponsel atau Email</label>
		        					<input type="text" name="" class="form-control">
		        					<label  style="font-size: 11px;color: grey">Contoh: email@kangenkamu.com</label>
        						</div>

        						<!-- email -->
								<div >
		        					<label  style="font-size: 12px;color: grey">Nomor Ponsel</label>
		        					<input type="number" name="" class="form-control">
		        					<label  style="font-size: 11px;color: grey">Contoh: 0813456786</label>
        						</div>

        						<div >
		        					<label  style="font-size: 12px;color: grey">Password</label>
		        					<input type="password" name="" class="form-control">
        						</div>

        						<!-- email -->
								<div>
									<div style="margin-top: 2%"></div>
									<button class="btn btn-outline-success" style="width: 100%">Daftar</button>
									<label  style="font-size: 11px;color: grey">*Menyetujui Syarat Dan ketentuan</label>
        						</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="div-register-image"> 
						<img src="https://ecs7.tokopedia.net/img/content/register_new.png" class="register-image">
						<center><label class="text-register-image">Ingin Barang Zaman Kecil Anda </label>
								<label>Gabung dan rasakan transaksi di kangen.com</label>
						</center>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<footer class="footer-div">
		

</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../kangenkampung/js/slick.min.js"></script>
	<script src="../kangenkampung/js/easyzoom.js"></script>
	<script src="../kangenkampung/js/kangenkampung.js"></script>
</html>

