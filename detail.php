<!DOCTYPE html>
<html>
<link rel="stylesheet" href="../kangenkampung/css/kangenkampung.css">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="../kangenkampung/css/slick-theme.css"/>
<link rel="stylesheet" href="../kangenkampung/css/pygments.css" />
<link rel="stylesheet" href="../kangenkampung/css/easyzoom.css" />
<head>
	<title></title>
</head>
<body class="background-body">
	<nav class="navbar-alert fixed-top" id="navbar-alert">

		<div class="navbar-header navbar-pointer scroll-hide">
				    <div Class="navbar-left">
				    	<div class="navbar-contact">
                        Call us on (504) 305-0554
                    </div>
				    </div>
				    <div class="navbar-right">
				    	<div class="row">
                            <div class="col-sm-1 mr7">
                                Home
                            </div>
                            <div class="col-sm-1 mr7">
                                About 
                            </div>
                            <div class="col-sm-1">
                                Contact
                            </div>
                    </div>
				    </div>
		  </div>

		 
		  <div class="navbar-body" >
		  	<div class="row">
		  		<div class="col-sm-2" style="margin-top: 15px;margin-left: 12px">
					  	<div class="logo-header" >
					  			<img src="../kangenkampung/images/logo.png">
					  	</div>	
		  		</div>
		  		<div class="col-sm-8" style="margin-top: 18px;">
				  	<div class="input-group">
					      <div class="input-group-prepend">
										    <select name="product_cat" class="dropdown_product_cat"><option value="">Select a category</option>	<option class="level-0" value="camera">Camera&nbsp;(4)</option>
												<option class="level-0" value="cooking">Cooking&nbsp;(1)</option>
												<option class="level-0" value="fashion">Fashion&nbsp;(1)</option>
												<option class="level-0" value="gadget">Gadget&nbsp;(3)</option>
												<option class="level-0" value="helmet">Helmet&nbsp;(2)</option>
												<option class="level-0" value="laptop">Laptop&nbsp;(2)</option>
												<option class="level-0" value="sport" selected="selected">Sport&nbsp;(2)</option>
											</select>
					      </div>
					      <input type="text" class="form-control search-box" placeholder="Cari Barang anda di sini ...">
					       <div class="input-group-prepend">
					       	 <span class="input-group-text search-icon"><i class="fa fa-search"></i></span>
					       </div>
					    </div>
				  	</div>
		  		<div class="col-sm-1"  style="margin-top: 11px;">
		  				<div class="btn-group">
					  <button type="button" class="navbar-login">Masuk</button>
					  <button type="button" class="navbar-daftar">Daftar</button>
					</div>
		  		s</div>
		  	</div>
		  </div>

		  <div class="appointment" style="text-align: left">
		  <div class="container">
		 		 Menu / home / Detail
		  </div>
		  </div>
	</nav>
	<div class="content">
			<!-- Body -->
			<div class="container" style="margin-top: 13%"> 
			<div class="row" >
					<div class="col-sm-6">
						<div class="row">
							<!-- Image Max 5 -->
							<div class="col-sm-3">
							<?php for ($i=0; $i <5 ; $i++) {?>
									<div class="border-detail-preview-child navbar-pointer" >
									<div class="img-hover-zoom ">
										<img src="http://demoapus-wp.com/tumbas/wp-content/uploads/2016/12/2-3.jpg" class="image-detail-preview-child" onmouseover="preview_detail('<?php echo "$i"; ?>')" id="<?php echo "$i"; ?>">
										</div>
									</div>
							<?php } ?>
							</div>	
							<div class="col-sm-6">
							<div class="easyzoom easyzoom--overlay">
								<img src="https://ecs7.tokopedia.net/img/cache/700/product-1/2019/4/5/21722219/21722219_8b21c41c-fd79-4d65-ae78-e91468882464_600_600.jpg" class="image-detail-preview" id="preview-detail">

								 <div class="top-left">
								 	<span class="onsale">Sale !</span>
								 </div>
							</div>
							</div>
						</div>
					</div>
					<!-- Detail Product -->
					<div class="col-sm-6" >
						<div class="row">
								<div class="col-sm-8" style="margin-top: 1%">
									<span class="detail-product-title">Helmet Winter Sport</span>
									<div class="row">
										<div class="col-sm-6">
											<label class="jenis-detail-product">Jenis : </label>
											<label>Makanan</label>
										</div>
										<div class="col-sm-6">
											<label class="jenis-detail-product">Stok :</label>
											<label >21 pcs </label>
										</div>
									</div>
									<div>
											<label style="font-weight: bold;">Khas :</label>
											<label >Kota Wisata batu </label>
									</div>
									<div>
										<label>99.29%  Transaksi Sukses Dari  >100  Transaksi </label>
									</div>
									<div class="detail-price">
										Rp.20.000
									</div>

									<div>
										<div class="row" style="margin-top: 5%;">
											<div class="col-sm-6">
												<label><b>Jumlah</b></label>
												<div class="row" style="margin-left: 1%">
													<div>
														<input type="number" class="form-control" name="" style="width:50%" value="1">
													</div>
												</div>
											</div>

											<div class="col-sm-6" >
												<label><b>Catatan ke penjual</b></label>
												<input type="text" class="form-control" name="">
											</div>											
										</div>
									</div>

									<div style="margin-top: 5%">
										<label><b>Estimasi ongkos kirim</b></label>
										<div>
											<table class="table table-bordered">
												<tr class="table-detail-title">
													<td>Kecamatan</td>
													<td>Kode Pos</td>
													<td>Berat</td>
													<td rowspan="2" style="vertical-align : middle;text-align:center;" ><input type="button" class="navbar-daftar" value="Hitung"></td>
												</tr>
												<tr>
													<td><select class="form-control"></select></td>
													<td><select class="form-control"></select></td>
													<td><label>50gr</label></td>
												</tr>

											</table>
										</div>
									</div>
								</div>
								<div class="col-sm-4" style="">
									<div class="detail-catatan-toko">
										<div class="title-catatan-toko"><label>Catatan Toko</label></div>
										<div  class="body-catatan-toko"> 
											<label>beli barang ini denganbijak dan gunakan sebaik baiknya jangan di buat lainnya</label>
										</div>	
									</div>

									<div class="detail-pengiriman-toko" style="">
										<div class="title-catatan-toko"><label> Partner pengiriman</label></div>
										<div  class=""> 
										<img src="https://ecs7.tokopedia.net/img/kurir-grab.png" class="detail-courir-logo" >
											<div class="row">
												<div class="col-sm-3"  style="margin-left: 8px">
													<label style="font-size: 11px">Oke</label>
												</div>
												<div class="col-sm-3">
													<label style="font-size: 11px">Express</label>
												</div>
											</div>
										</div>	

										<div  class=""> 
										<img src="https://ecs7.tokopedia.net/img/kurir-custom.png" >
											<div class="row">
												<div class="col-sm-3"  style="margin-left: 8px">
													<label style="font-size: 11px">Oke</label>
												</div>
												<div class="col-sm-3">
													<label style="font-size: 11px">Express</label>
												</div>
											</div>
										</div>	
									</div>
								</div>
						</div>
					</div>
			</div>
			</div>

			<!-- Description -->

			<div class="detail-description-box ">
			<div class="container">
					<ul class="nav nav-tabs " role="tablist">
					  <li class="nav-item navbar-pointer active">
					    <a class="nav-link  " href="#informasi" role="tab" data-toggle="tab"><label class="primary-color navbar-pointer">Informasi Product</label></a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link navbar-pointer" href="#ulasan" role="tab" data-toggle="tab"><label class="primary-color navbar-pointer">Ulasan</label></a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link navbar-pointer" href="#diskusi" role="tab" data-toggle="tab"><label class="primary-color navbar-pointer">Diskusi Produk</label></a>
					  </li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content" style="color: black">
					  <div role="tabpanel" class="tab-pane active" id="informasi">
					  	<div style="padding: 2%">
					  		Ini Halaman Home
					  	</div>
					  </div>
					  <div role="tabpanel" class="tab-pane fade" id="ulasan">
					  	<div style="padding: 2%">
					  		Ini Halaman Home
					  	</div>
					  </div>
					  <div role="tabpanel" class="tab-pane fade" id="diskusi">
					  		<div style="padding: 2%">
					  		Ini Halaman Home
					  	</div>
					  </div>
					</div>
			</div>
			</div>
			<!-- Caraousel inFooter -->
			<br><br><br>
				<div class="card" style="margin-left: 3%;margin-right: 3%">
			<div class="container" style="display: block">
		
            <div class="row">
            	<div class="col-sm-6" style="margin-top: 3%">
			<p  class="title-kategory">Rekomendasi Untuk kamu</p>
			</div>
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top: 3%">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                            	<?php for ($i=0; $i <6 ; $i++) {?>
                                <div class="card-item-relation">
                                   <div class="card">
										<div class="img-hover-zoom ">
												<img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/7/12/1618811/1618811_5011e015-f0c6-477a-b330-61f425bb6a11_719_873.jpeg" style="width: 100%">
												<div class="top-right"><div class="price-diskon">26% OFF</div></div>
										</div>
						 				<div style="text-align: left;padding: 6px">
						 						<div style="" class="title-item"><b>[Premium Quality] Case Xiaomi Redmi Note 7</b></div>
						 						<div style="" class="primary-color price-item"><b>Rp.2222</b></div>
						 						<div style="" class="location-item">Jakarta</div>
					 	 				</div>

					 	 		</div>
                                </div> 
                                <?php } ?>
                            </div>
                        </div>



                        <div class="carousel-item">
                            <div class="row">
                            <?php for ($i=0; $i <6 ; $i++) {?>
                                 <div class="card-item-relation">
                                   <div class="card">
										<div class="img-hover-zoom ">
												<img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/7/12/1618811/1618811_5011e015-f0c6-477a-b330-61f425bb6a11_719_873.jpeg" style="width: 100%">
												<div class="top-right"><div class="price-diskon">26% OFF</div></div>
										</div>
						 				<div style="text-align: left;padding: 6px">
						 						<div style="" class="title-item"><b>[Premium Quality] Case Xiaomi Redmi Note 7</b></div>
						 						<div style="" class="primary-color price-item"><b>Rp.2222</b></div>
						 						<div style="" class="location-item">Jakarta</div>
					 	 				</div>

					 	 		</div>
                                </div>  
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <br>
            </div>    
        </div>
	</div>
	</div>
</body>
<footer class="footer-div">
			<div>
					<img src="../kangenkampung/images/logo.png" class="logo-footer">
			</div>	
			<div style="margin-top: 2%">
				<label>120 Jasmine Ln, Westwego, LA, 70094</label><br>
				<label>(504) 305-0554 &nbsp;&nbsp;</label><label class="primary-color">hello@tumbas.com</label>
			</div>
			<div class="hr-footer">
				<hr>
			</div>		
				
			

			<nav class="navbar-bottom fixed-bottom" style="display: block">
				<div class="navbar-body-bottom" >
				  	<div class="row">
				  		<div class="col-sm-8">
				  		<div class="row">
				  			<div class="col-sm-8" style="text-align: left;">
				  				<div style="margin-left: 12%">

				  				</div>
				  			</div>
				  			<div class="col-sm-4" style="text-align: right;">
				  				<div class="total-childe-text" >TOTAL</div>
				  				<label class="total-text primary-color">Rp.20.000</label>
				  			</div>
				  			
				  		</div>
				  		</div>
				  		<div class="col-sm-4" >
				  		<div class="form-group" style="text-align: right;margin-right: 15%">
				  				<a href="keranjang.php"><button  class="btn btn-outline-success btn-lg" style="min-width: 120px;">Beli</button></a>
				  				<button class="btn btn-success btn-lg cart-btn-text" style="min-width: 120px;">Masukkan Keranjang</button>
				  			</div>
				  		</div>
				  	</div>
				  </div>
			</nav>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../kangenkampung/js/slick.min.js"></script>
	<script src="../kangenkampung/js/easyzoom.js"></script>
	<script src="../kangenkampung/js/kangenkampung.js"></script>
</html>

